package com.ms.services.configurations;

import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
@EnableSwagger2WebMvc
public class SwaggerConfiguration {

	@Bean
    public Docket usersApi() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.directModelSubstitute(LocalDateTime.class, Date.class)
                .apiInfo(usersApiInfo())
                .select()
                .paths(PathSelectors.ant("/api/**"))
                .apis(RequestHandlerSelectors.basePackage("com.ms.services.controllers"))
                .build()
                .useDefaultResponseMessages(false);
    }

	private ApiInfo usersApiInfo() {
        return new ApiInfoBuilder()
                .title("Base para un micro servicio con swagger")
                .version("1.0")
                .license("Ricardo Muñoz")
                .build();
    }
	

}
