package com.ms.services.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ms.services.entities.Region;

public interface RegionRepository extends JpaRepository<Region, Long>{

}
