package com.ms.services.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ms.services.entities.Region;
import com.ms.services.repositories.RegionRepository;

@Service
public class RegionService {
	
	@Autowired
	private RegionRepository regionRepository;
	
	public List<Region> getListRegion(){
		return this.regionRepository.findAll();
	}
	
	public Region insertRegion(Region region) {
		this.regionRepository.save(region);
		return region;
	}
}
