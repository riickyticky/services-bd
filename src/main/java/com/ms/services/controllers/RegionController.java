package com.ms.services.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.ms.services.entities.Region;
import com.ms.services.services.RegionService;

@RestController
@RequestMapping("/api")
public class RegionController {
	
	@Autowired
	private RegionService regionService;
	
	@GetMapping(value = "traer-lista-region", produces = { MediaType.APPLICATION_JSON_VALUE })
	public List<Region> getListRegion(){
		return regionService.getListRegion();
	}
	
	@PostMapping(value = "insertar-region", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Region insertRegion(@RequestBody Region region) {
		regionService.insertRegion(region);
		return region;
	}
}
